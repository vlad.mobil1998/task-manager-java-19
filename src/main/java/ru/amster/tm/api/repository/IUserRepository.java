package ru.amster.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.amster.tm.entity.User;

import java.util.List;

public interface IUserRepository {

    @NotNull
    List<User> getUsers();

    void removeAll();

    @NotNull
    List<User> findAll();

    @NotNull
    void add(@NotNull User user);

    @NotNull
    User findById(@NotNull String id);

    @NotNull
    User findByLogin(@NotNull String login);

    @NotNull
    User removeUser(@NotNull User user);

    @NotNull
    User removeById(@NotNull String id);

    @NotNull
    User removeByLogin(@NotNull String login);

}