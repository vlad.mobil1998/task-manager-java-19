package ru.amster.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.entity.Project;

import java.util.List;

public interface IProjectRepository {

    @NotNull
    List<Project> getProjects();

    void add(@NotNull Project project);

    void removeAll();

    void remove(@NotNull String userId, @NotNull Project project);

    @NotNull
    List<Project> findAll(@NotNull String userId);

    void removeAll(String userId);

    @NotNull
    Project findOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project findOneByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Project findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Project removeOneByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Project removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Project removeOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Integer numberOfAllProjects(@NotNull String userId);

}