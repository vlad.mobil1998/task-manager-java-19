package ru.amster.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {

    @NotNull
    List<Task> getTasks();

    void add(@NotNull Task task);

    void remove(@Nullable String userId, @NotNull Task task);

    void removeAll();

    @NotNull
    List<Task> findAll(@Nullable String userId);

    void removeAll(@Nullable String userId);

    @NotNull
    Task findOneById(@Nullable String userId, @NotNull String id);

    @NotNull
    Task findOneByName(@Nullable String userId, @NotNull String name);

    @NotNull
    Task findOneByIndex(@Nullable String userId, @NotNull Integer index);

    @NotNull
    Task removeOneByName(@Nullable String userId, @NotNull String name);

    @NotNull
    Task removeOneByIndex(@Nullable String userId, @NotNull Integer index);

    @NotNull
    Task removeOneById(@Nullable String userId, @NotNull String id);

    @NotNull
    Integer numberOfAllTasks(@Nullable String userId);
}