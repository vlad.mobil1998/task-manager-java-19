package ru.amster.tm.api.servise;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.entity.User;
import ru.amster.tm.enamuration.Role;

import java.util.List;

public interface IUserService {

    @NotNull
    List<User> findAll();

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email,@Nullable Role role);

    @NotNull
    User findById(@Nullable String id);

    @NotNull
    User findByLogin(@Nullable String login);

    @NotNull
    User removeUser(@Nullable User user);

    @NotNull
    User removeById(@Nullable String id);

    @NotNull
    User removeByLogin(@Nullable String login);

    @NotNull
    User updateEmail(@NotNull String id, @Nullable String email);

    @NotNull
    User updateFirstName(@NotNull String id, @Nullable String firstName);

    @NotNull
    User updateLastName(@NotNull String id, @Nullable String lastName);

    @NotNull
    User updateMiddleName(@NotNull String id, @Nullable String middleName);

    @NotNull
    User updatePassword(@NotNull String id, @Nullable String password);

    @NotNull
    User lockUserByLogin(@Nullable String login);

    @NotNull
    User unlockUserByLogin(@Nullable String login);

    void load(List<User> users);

    @NotNull
    List<User> export();

}