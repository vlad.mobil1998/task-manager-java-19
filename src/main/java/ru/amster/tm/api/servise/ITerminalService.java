package ru.amster.tm.api.servise;

import org.jetbrains.annotations.NotNull;
import ru.amster.tm.command.AbstractCommand;

import java.util.Collection;

public interface ITerminalService extends ServiceLocator {

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull
    Collection<AbstractCommand> getArguments();

    @NotNull
    AbstractCommand getCommand(@NotNull String cmd);

    @NotNull
    AbstractCommand getArgument(@NotNull String arg);

    @NotNull
    @Override
    IAuthenticationService getAuthService();

    @NotNull
    @Override
    IUserService getUserService();

    @NotNull
    @Override
    ITaskService getTaskService();

    @NotNull
    @Override
    IProjectService getProjectService();

    @NotNull
    @Override
    IDomainService getDomainService();

}