package ru.amster.tm.api.servise;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.entity.Task;

import java.util.List;

public interface ITaskService {

    void create(@Nullable String userId, @Nullable  String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    void add(@Nullable String userId, @Nullable Task project);

    void remove(@Nullable String userId, @Nullable Task project);

    @NotNull
    List<Task> findAll(@Nullable String userId);

    void clear(@Nullable String userId);

    @NotNull
    Task findOneById(@Nullable String userId, @Nullable String id);

    @NotNull
    Task findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Task findOneByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Task removeOneByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Task removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Task removeOneById(@Nullable String userId, @Nullable String id);

    @NotNull
    Task updateTaskById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    Task updateTaskByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NotNull
    Integer numberOfAllTasks(@Nullable String userId);

    void load(@Nullable List<Task> tasks);

    @NotNull
    List<Task> export();

}