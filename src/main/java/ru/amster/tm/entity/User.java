package ru.amster.tm.entity;

import lombok.Getter;
import lombok.Setter;
import ru.amster.tm.enamuration.Role;

@Getter
@Setter
public class User extends AbstractEntity {

    private String login;

    private String passwordHash;

    private String email;

    private String fistName;

    private String lastName;

    private String middleName;

    private Role role = Role.USER;

    private Boolean Locked = false;

}