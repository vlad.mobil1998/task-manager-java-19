package ru.amster.tm.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class Task extends AbstractEntity {

    private String name;

    private String description;

    private String UserId;

    @Override
    public String toString() {
        return getId() + ": " + name;
    }

}