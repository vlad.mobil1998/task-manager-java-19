package ru.amster.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.servise.ITerminalService;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.exception.system.InvalidArgumentException;
import ru.amster.tm.exception.system.InvalidCommandException;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.service.TerminalService;
import ru.amster.tm.util.TerminalUtil;

import java.io.IOException;

public class Bootstrap {

    private final ITerminalService terminalService = new TerminalService();

    private void initUsers() {
        terminalService.getUserService().create("test", "test", "test@test.ru");
        terminalService.getUserService().create("test1", "test1", "test@test.ru");
        terminalService.getUserService().create("admin", "admin", Role.ADMIN);
    }

    public final void run(@NotNull final String[] args) throws IOException, ClassNotFoundException {
        System.out.println("**** WELCOME TO TASK MANAGER ****");
        if (parseArgs(args)) System.exit(0);
        initUsers();
        while (true) {
            try {
                parseCmd(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.out.println("[FAIL]");
            }
        }
    }

    private void parseCmd(@NotNull final String cmd) throws IOException, ClassNotFoundException {
        if (cmd == null || cmd.isEmpty()) return;
        @Nullable final AbstractCommand command = terminalService.getCommand(cmd);
        if (command == null) throw new InvalidCommandException();
        terminalService.getAuthService().checkRoles(command.roles());
        command.execute();
    }

    private boolean parseArgs(@Nullable final String[] args) throws IOException, ClassNotFoundException {
        if (args == null || args.length == 0) return false;
        for (@NotNull String arg : args) {
            System.out.println(" ");
            @Nullable final AbstractCommand argument = terminalService.getArgument(arg);
            if (argument == null) throw new InvalidArgumentException();
            argument.execute();
        }
        return true;
    }

}