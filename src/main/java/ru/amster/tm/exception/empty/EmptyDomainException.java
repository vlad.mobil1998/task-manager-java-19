package ru.amster.tm.exception.empty;

import ru.amster.tm.exception.AbstractException;

public class EmptyDomainException extends AbstractException {

    public EmptyDomainException() {
        super("ERROR! Domain is empty...");
    }
}
