package ru.amster.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.amster.tm.exception.system.InvalidIndexException;

import java.util.Scanner;

@UtilityClass
public class TerminalUtil {

    @NotNull
    final Scanner SCANNER = new Scanner(System.in);

    @NotNull
    public String nextLine() {
        return SCANNER.nextLine();
    }

    public Integer nextNumber() {
        @NotNull final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            throw new InvalidIndexException(value);
        }
    }

}