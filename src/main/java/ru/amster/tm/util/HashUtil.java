package ru.amster.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import lombok.experimental.UtilityClass;
import ru.amster.tm.exception.empty.EmptyValueException;

@UtilityClass
public class HashUtil {

    @NotNull
    final String SECRET = "12sasd213";

    @NotNull
    final Integer ITERATOR = 32768;

    @NotNull
    public String salt(@Nullable final String value) {
        if (value == null) throw new EmptyValueException();
        @NotNull String result = value;
        for (int i = 0; i < ITERATOR; i++)
            result = md5(SECRET + result + SECRET);
        return result;
    }

    @Nullable
    private String md5(@NotNull final String value) {
        try {
            @NotNull final java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            @NotNull final byte[] array = md.digest(value.getBytes());
            @NotNull final StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}