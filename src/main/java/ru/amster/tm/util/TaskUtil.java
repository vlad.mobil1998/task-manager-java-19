package ru.amster.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import lombok.experimental.UtilityClass;
import ru.amster.tm.entity.Task;
import ru.amster.tm.exception.empty.EmptyTaskException;

@UtilityClass
public class TaskUtil {

    @NotNull
    public void showTask(@Nullable final Task task) {
        if (task == null) throw new EmptyTaskException();
        System.out.println("ID: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
    }

}