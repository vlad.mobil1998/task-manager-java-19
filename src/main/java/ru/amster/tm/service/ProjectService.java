package ru.amster.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.repository.IProjectRepository;
import ru.amster.tm.api.servise.IProjectService;
import ru.amster.tm.entity.Project;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.empty.EmptyProjectException;
import ru.amster.tm.exception.system.InvalidIndexException;
import ru.amster.tm.exception.user.AccessDeniedException;

import java.util.List;

public class ProjectService implements IProjectService {

    @NotNull
    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Project project = new Project();
        project.setName(name);
        add(userId, project);
    }

    @Override
    public void create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return;
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        add(userId, project);
    }

    @Override
    public void add(@Nullable final String userId, @Nullable Project project) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (project == null) throw new EmptyProjectException();
        project.setUserId(userId);
        projectRepository.add(project);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final Project project) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (project == null) throw new EmptyProjectException();
        projectRepository.remove(userId, project);
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return projectRepository.findAll(userId);
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        projectRepository.removeAll(userId);
    }

    @NotNull
    @Override
    public Project findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findOneById(userId, id);
    }

    @NotNull
    @Override
    public Project findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new InvalidIndexException();
        return projectRepository.findOneByIndex(userId, index);
    }

    @NotNull
    @Override
    public Project findOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findOneByName(userId, name);
    }

    @NotNull
    @Override
    public Project removeOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.removeOneByName(userId, name);
    }

    @NotNull
    @Override
    public Project removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new InvalidIndexException();
        return projectRepository.removeOneByIndex(userId, index);
    }

    @NotNull
    @Override
    public Project removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.removeOneById(userId, id);
    }

    @NotNull
    @Override
    public Project updateProjectById(
            @Nullable final String userId, @Nullable final String id,
            @Nullable final String name, @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new EmptyProjectException();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @NotNull
    @Override
    public Project updateProjectByIndex(
            @Nullable final String userId, @Nullable final Integer index,
            @Nullable final String name, @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new InvalidIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new EmptyProjectException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @NotNull
    @Override
    public Integer numberOfAllProjects(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return projectRepository.numberOfAllProjects(userId);
    }

    @Override
    public void load(@NotNull final List<Project> projects) {
        projectRepository.removeAll();
        for (@NotNull Project project: projects) {
            add(project.getUserId(), project);
        }
    }

    @NotNull
    public List<Project> export() {
        return projectRepository.getProjects();
    }

}