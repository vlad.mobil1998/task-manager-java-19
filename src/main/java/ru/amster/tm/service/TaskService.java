package ru.amster.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.repository.ITaskRepository;
import ru.amster.tm.api.servise.ITaskService;
import ru.amster.tm.entity.Task;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.empty.EmptyTaskException;
import ru.amster.tm.exception.system.InvalidIndexException;
import ru.amster.tm.exception.user.AccessDeniedException;

import java.util.List;

public class TaskService implements ITaskService {

    @NotNull
    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null && name.isEmpty()) throw new EmptyNameException();
        @NotNull final Task task = new Task();
        task.setName(name);
        add(userId, task);
    }

    @Override
    public void create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null && name.isEmpty()) throw new EmptyNameException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        add(userId, task);
    }

    @Override
    public void add(@Nullable final String userId, @Nullable final Task task) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        task.setUserId(userId);
        if (task == null) throw new EmptyTaskException();
        taskRepository.add(task);
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return taskRepository.findAll(userId);
    }

    @NotNull
    @Override
    public Task findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new InvalidIndexException();
        return taskRepository.findOneByIndex(userId, index);
    }

    @NotNull
    @Override
    public Task findOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findOneByName(userId, name);
    }

    @NotNull
    @Override
    public Task findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findOneById(userId, id);
    }

    @NotNull
    @Override
    public Task removeOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.removeOneByName(userId, name);
    }

    @NotNull
    @Override
    public Task removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new InvalidIndexException();
        return taskRepository.removeOneByIndex(userId, index);
    }

    @NotNull
    @Override
    public Task removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.removeOneById(userId, id);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final Task task) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (task == null) throw new EmptyTaskException();
        taskRepository.remove(userId, task);
    }

    @NotNull
    @Override
    public Task updateTaskById(
            @Nullable final String userId, @Nullable final String id,
            @Nullable final String name, @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new EmptyTaskException();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @NotNull
    @Override
    public Task updateTaskByIndex(
            @Nullable final String userId, @Nullable final Integer index,
            @Nullable final String name, @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new InvalidIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) throw new EmptyTaskException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        taskRepository.removeAll(userId);
    }

    @NotNull
    @Override
    public Integer numberOfAllTasks(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return taskRepository.numberOfAllTasks(userId);
    }

    @Override
    public void load(@Nullable final List<Task> tasks) {
        taskRepository.removeAll();
        for (@NotNull Task task: tasks) {
            add(task.getUserId(), task);
        }
    }

    @NotNull
    public List<Task> export() {
        return taskRepository.getTasks();
    }

}