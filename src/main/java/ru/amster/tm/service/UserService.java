package ru.amster.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.repository.IUserRepository;
import ru.amster.tm.api.servise.IUserService;
import ru.amster.tm.entity.User;
import ru.amster.tm.exception.empty.*;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.util.HashUtil;

import java.util.List;

public class UserService implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(role);
        userRepository.add(user);
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final User user = new User();
        if (user == null) throw new EmptyUserException();
        user.setLogin(login);
        user.setEmail(email);
        user.setPasswordHash(HashUtil.salt(password));
        userRepository.add(user);
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login, @Nullable final String password,
            @Nullable final String email, @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final User user = new User();
        if (user == null) throw new EmptyUserException();
        user.setLogin(login);
        user.setEmail(email);
        user.setRole(role);
        user.setPasswordHash(HashUtil.salt(password));
        userRepository.add(user);
        return user;
    }

    @NotNull
    @Override
    public User findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @NotNull
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @NotNull
    @Override
    public User removeUser(@Nullable final User user) {
        if (user == null) throw new EmptyUserException();
        return userRepository.removeUser(user);
    }

    @NotNull
    @Override
    public User removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.removeById(id);
    }

    @NotNull
    @Override
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.removeByLogin(login);
    }

    @NotNull
    @Override
    public User updateEmail(@NotNull final String id, @Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable final User user = findById(id);
        if (user == null) throw new EmptyUserException();
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public User updateFirstName(@NotNull final String id, @Nullable final String firstName) {
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();
        @Nullable final User user = findById(id);
        if (user == null) throw new EmptyUserException();
        user.setFistName(firstName);
        return user;
    }

    @NotNull
    @Override
    public User updateLastName(@NotNull final String id, @Nullable final String lastName) {
        if (lastName == null || lastName.isEmpty()) throw new EmptyLastNameException();
        @Nullable final User user = findById(id);
        if (user == null) throw new EmptyUserException();
        user.setLastName(lastName);
        return user;
    }

    @NotNull
    @Override
    public User updateMiddleName(@NotNull final String id, @Nullable final String middleName) {
        if (middleName == null || middleName.isEmpty()) throw new EmptyMiddleNameException();
        @Nullable final User user = findById(id);
        if (user == null) throw new EmptyUserException();
        user.setMiddleName(middleName);
        return user;
    }

    @NotNull
    @Override
    public User updatePassword(@NotNull final String id, @Nullable final String password) {
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = findById(id);
        if (user == null) throw new EmptyUserException();
        @NotNull final String passwordUpd = HashUtil.salt(password);
        user.setPasswordHash(passwordUpd);
        return user;
    }

    @NotNull
    @Override
    public User lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new EmptyUserException();
        user.setLocked(true);
        return user;
    }

    @NotNull
    @Override
    public User unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new EmptyUserException();
        user.setLocked(false);
        return user;
    }

    @Override
    public void load(@NotNull final List<User> users) {
        userRepository.removeAll();
        for (@NotNull User user: users) {
            userRepository.add(user);
        }
    }

    @NotNull
    @Override
    public List<User> export() {
        return userRepository.getUsers();
    }

}