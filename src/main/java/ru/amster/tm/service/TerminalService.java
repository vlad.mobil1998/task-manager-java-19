package ru.amster.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.amster.tm.api.repository.IProjectRepository;
import ru.amster.tm.api.repository.ITaskRepository;
import ru.amster.tm.api.repository.IUserRepository;
import ru.amster.tm.api.servise.*;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.command.authentication.*;
import ru.amster.tm.command.data.base64.DataBase64ClearCommand;
import ru.amster.tm.command.data.base64.DataBase64LoadCommand;
import ru.amster.tm.command.data.base64.DataBase64SaveCommand;
import ru.amster.tm.command.data.binary.DataBinaryClearCommand;
import ru.amster.tm.command.data.binary.DataBinaryLoadCommand;
import ru.amster.tm.command.data.binary.DataBinarySaveCommand;
import ru.amster.tm.command.data.json.DataJsonClearCommand;
import ru.amster.tm.command.data.json.DataJsonLoadCommand;
import ru.amster.tm.command.data.json.DataJsonSaveCommand;
import ru.amster.tm.command.data.xml.DataXmlClearCommand;
import ru.amster.tm.command.data.xml.DataXmlLoadCommand;
import ru.amster.tm.command.data.xml.DataXmlSaveCommand;
import ru.amster.tm.command.project.*;
import ru.amster.tm.command.system.*;
import ru.amster.tm.command.task.*;
import ru.amster.tm.command.user.*;
import ru.amster.tm.command.user.update.*;
import ru.amster.tm.repository.ProjectRepository;
import ru.amster.tm.repository.TaskRepository;
import ru.amster.tm.repository.UserRepository;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class TerminalService implements ITerminalService {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthenticationService authService = new AuthenticationService(userService);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IDomainService domainService = new DomainService(
            projectService, taskService, userService
    );

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @NotNull
    private final UserActivated userActivated = new UserActivated();

    @NotNull
    private static final Class[] COMMANDS = new Class[]{
            SystemHelpCommand.class, SystemAboutCommand.class, SystemInfoCommand.class,
            SystemVersionCommand.class, SystemCommandCommand.class, SystemArgumentCommand.class,

            EmailUpdateCommand.class, FirstNameUpdateCommand.class, LastNameUpdateCommand.class,
            MiddleNameUpdateCommand.class, AllUserShowCommand.class, PasswordUpdateCommand.class,
            UserUnlockCommand.class, UserLockCommand.class, UserRemoveCommand.class, UserProfileShowCommand.class,

            ProjectCreateCommand.class, ProjectShowByIndexCommand.class, ProjectShowByNameCommand.class,
            ProjectShowByIdCommand.class, ProjectShowCommand.class, ProjectUpdateByIdCommand.class,
            ProjectUpdateByIndexCommand.class, ProjectRemoveByIdCommand.class,
            ProjectRemoveByNameCommand.class, ProjectRemoveByIndexCommand.class, ProjectClearCommand.class,

            TaskCreateCommand.class, TaskShowByIndexCommand.class, TaskShowByNameCommand.class,
            TaskShowByIdCommand.class, TaskShowCommand.class, TaskUpdateByIdCommand.class,
            TaskUpdateByIndexCommand.class, TaskRemoveByIdCommand.class, TaskRemoveByNameCommand.class,
            TaskRemoveByIndexCommand.class, TaskClearCommand.class,

            DataBinarySaveCommand.class, DataBinaryClearCommand.class,
            DataBase64SaveCommand.class, DataBase64ClearCommand.class,
            DataJsonSaveCommand.class, DataJsonClearCommand.class,
            DataXmlSaveCommand.class, DataXmlClearCommand.class,

            SystemExitCommand.class,
    };

    @NotNull
    private static final Class[] COMMANDS_AUTH = new Class[]{
            LoginCommand.class, LogoutCommand.class, RegistrationCommand.class,
            DataBinaryLoadCommand.class, DataBase64LoadCommand.class,
            DataJsonLoadCommand.class, DataXmlLoadCommand.class,
    };

    @NotNull
    private static final Class[] ARGUMENTS = new Class[]{
            SystemHelpCommand.class, SystemAboutCommand.class,
            SystemInfoCommand.class, SystemVersionCommand.class,
    };

    {
        for (@NotNull final Class clazz : COMMANDS) {
            try {
                @NotNull final Object commandInstance = clazz.newInstance();
                @NotNull final AbstractCommand command = (AbstractCommand) commandInstance;
                command.setServiceLocator(this);
                commands.put(command.name(), command);
            } catch (Exception e) {
                throw new RuntimeException();
            }
        }
    }

    {
        for (@NotNull final Class clazz : COMMANDS_AUTH) {
            try {
                @NotNull final Object commandInstance = clazz.newInstance();
                @NotNull final AbstractCommand command = (AbstractCommand) commandInstance;
                command.setServiceLocator(this);
                @NotNull final AbstractAuthCommand commandAuth = (AbstractAuthCommand) command;
                commandAuth.setLoginCheck(userActivated);
                commands.put(command.name(), command);
            } catch (Exception e) {
                throw new RuntimeException();
            }
        }
    }

    {
        for (@NotNull final Class clazz : ARGUMENTS) {
            try {
                @NotNull final Object commandInstance = clazz.newInstance();
                @NotNull final AbstractCommand argument = (AbstractCommand) commandInstance;
                arguments.put(argument.arg(), argument);
            } catch (Exception e) {
                throw new RuntimeException();
            }
        }
    }

    @NotNull
    @Override
    public IAuthenticationService getAuthService() {
        return authService;
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public IDomainService getDomainService() {
        return domainService;
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @NotNull
    @Override
    public AbstractCommand getCommand(@NotNull final String cmd) {
        return commands.get(cmd);
    }

    @NotNull
    @Override
    public AbstractCommand getArgument(@NotNull final String arg) {
        return arguments.get(arg);
    }

}