package ru.amster.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.Task;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyTaskException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TaskUtil;
import ru.amster.tm.util.TerminalUtil;

public class TaskShowByIdCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "task-v-id";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Show task by id";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID");
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final String id = TerminalUtil.nextLine();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final Task task = serviceLocator.getTaskService().findOneById(userId, id);
        if (task == null) {
            System.out.println("[FAIL]");
            throw new EmptyTaskException();
        }
        TaskUtil.showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}