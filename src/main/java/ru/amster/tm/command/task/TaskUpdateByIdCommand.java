package ru.amster.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.Task;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.empty.EmptyTaskException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public class TaskUpdateByIdCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "task-upd-id";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Update task by id";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER ID");
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final String id = TerminalUtil.nextLine();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final Task task = serviceLocator.getTaskService().findOneById(userId, id);
        if (task == null) throw new EmptyTaskException();
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final Task taskUpdate = serviceLocator.getTaskService().updateTaskById(userId, id, name, description);
        if (taskUpdate == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}