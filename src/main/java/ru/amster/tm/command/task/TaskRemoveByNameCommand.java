package ru.amster.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.Task;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.empty.EmptyTaskException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public class TaskRemoveByNameCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "task-re-name";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Remove task by name";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER NAME");
        @Nullable final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final Task task = serviceLocator.getTaskService().removeOneByName(userId, name);
        if (task == null) {
            System.out.println("[FAIL]");
            throw new EmptyTaskException();
        }
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}