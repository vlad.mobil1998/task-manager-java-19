package ru.amster.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.Project;
import ru.amster.tm.exception.empty.EmptyProjectException;
import ru.amster.tm.exception.system.InvalidIndexException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public class ProjectRemoveByIndexCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "project-re-i";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Remove project by index";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER INDEX");
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final Integer maxIndex = serviceLocator.getProjectService().numberOfAllProjects(userId);
        if (index >= maxIndex) throw new InvalidIndexException(index, maxIndex);
        @Nullable final Project project = serviceLocator.getProjectService().removeOneByIndex(userId, index);
        if (project == null) {
            System.out.println("[FAIL]");
            throw new EmptyProjectException();
        }
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}