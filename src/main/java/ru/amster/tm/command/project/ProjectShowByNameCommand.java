package ru.amster.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.Project;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.empty.EmptyProjectException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.ProjectUtil;
import ru.amster.tm.util.TerminalUtil;

public class ProjectShowByNameCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "project-v-name";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Show project by name";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME");
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final Project project = serviceLocator.getProjectService().findOneByName(userId, name);
        if (project == null) {
            System.out.println("[FAIL]");
            throw new EmptyProjectException();
        }
        ProjectUtil.showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}