package ru.amster.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.servise.ITerminalService;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.service.TerminalService;

import java.util.Collection;

public class SystemCommandCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "commands";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Display terminal command";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        ITerminalService terminalService = new TerminalService();
        @NotNull final Collection<AbstractCommand> commands = terminalService.getCommands();
        for (AbstractCommand command : commands) {
            System.out.println(command.name());
        }
    }

}