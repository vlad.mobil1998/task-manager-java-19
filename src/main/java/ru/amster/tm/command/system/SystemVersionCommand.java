package ru.amster.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.amster.tm.command.AbstractCommand;

public class SystemVersionCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "version";
    }

    @Override
    @NotNull
    public String arg() {
        return "-v";
    }

    @Override
    @NotNull
    public String description() {
        return " - Show version info.";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.0.9");
    }

}