package ru.amster.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.amster.tm.command.AbstractCommand;

public class SystemAboutCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "about";
    }

    @Override
    @NotNull
    public String arg() {
        return "-a";
    }

    @Override
    @NotNull
    public String description() {
        return " - Show developer info.";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Amster Vladislav");
        System.out.println("E-MAIL: vlad@amster.ru");
    }

}