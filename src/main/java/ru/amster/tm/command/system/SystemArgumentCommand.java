package ru.amster.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.servise.ITerminalService;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.service.TerminalService;

import java.util.Collection;

public class SystemArgumentCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "arguments";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Display arguments program";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        ITerminalService terminalService = new TerminalService();
        @NotNull final Collection<AbstractCommand> arguments = terminalService.getArguments();
        for (AbstractCommand argument : arguments) {
            System.out.println(argument.arg());
        }
    }

}