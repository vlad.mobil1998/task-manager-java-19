package ru.amster.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.amster.tm.api.servise.ITerminalService;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.service.TerminalService;

import java.util.Collection;

public class SystemHelpCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "help";
    }

    @Override
    @NotNull
    public String arg() {
        return "-h";
    }

    @Override
    @NotNull
    public String description() {
        return " - Display terminal commands.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        ITerminalService terminalService = new TerminalService();
        @NotNull final Collection<AbstractCommand> commands = terminalService.getCommands();
        for (AbstractCommand command : commands) {
            if (command.arg() == null) System.out.println(command.name() + command.description());
            else System.out.println(command.name() + " " + command.arg() + command.description());
        }
    }

}