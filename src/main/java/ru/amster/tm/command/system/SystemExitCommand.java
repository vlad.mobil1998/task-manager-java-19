package ru.amster.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;

public class SystemExitCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "exit";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Close application.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}