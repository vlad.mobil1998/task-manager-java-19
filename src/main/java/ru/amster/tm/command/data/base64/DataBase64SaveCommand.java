package ru.amster.tm.command.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.data.AbstractDataCommand;
import ru.amster.tm.dto.Domain;
import ru.amster.tm.enamuration.Role;
import sun.misc.BASE64Encoder;

import java.io.*;
import java.nio.file.Files;

public class DataBase64SaveCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String name() {
        return "data-base64-save";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Save base64 data to file";
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException {
        System.out.println("[DATA BASE64 SAVE]");
        @NotNull final Domain domain = getDomain();

        @NotNull final File file = new File(FILE_BASE64);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();

        @NotNull final byte[] bytes = byteArrayOutputStream.toByteArray();
        @NotNull final String base64 = new BASE64Encoder().encode(bytes);
        byteArrayOutputStream.close();

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_BASE64);
        fileOutputStream.write(base64.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();

        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}