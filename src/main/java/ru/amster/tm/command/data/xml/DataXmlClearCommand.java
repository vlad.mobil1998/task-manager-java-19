package ru.amster.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.data.AbstractDataCommand;
import ru.amster.tm.enamuration.Role;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class DataXmlClearCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String name() {
        return "data-xml-clear";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Remove xml data";
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException {
        System.out.println("[DATA XML CLEAR]");
        @NotNull final File file = new File(FILE_XML);
        Files.delete(file.toPath());
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}