package ru.amster.tm.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.data.AbstractDataCommand;
import ru.amster.tm.dto.Domain;
import ru.amster.tm.enamuration.Role;
import sun.misc.BASE64Decoder;

import java.io.*;

public class DataJsonLoadCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String name() {
        return "data-json-load";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Load json data to file";
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException {
        System.out.println("[DATA JSON LOAD]");

        @NotNull final File file = new File(FILE_JSON);
        @NotNull final FileInputStream fileInputStream = new FileInputStream(file);

        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = objectMapper.readValue(file, Domain.class);

        setDomain(domain);
        fileInputStream.close();

        serviceLocator.getAuthService().logout();
        userActivated.setCheckForActivation(false);

        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}