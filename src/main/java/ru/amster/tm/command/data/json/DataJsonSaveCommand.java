package ru.amster.tm.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.data.AbstractDataCommand;
import ru.amster.tm.dto.Domain;
import ru.amster.tm.enamuration.Role;

import java.io.*;
import java.nio.file.Files;

public class DataJsonSaveCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String name() {
        return "data-json-save";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Save data from json file";
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException {
        System.out.println("[DATA JSON SAVE]");
        @NotNull final Domain domain = getDomain();

        @NotNull final File file = new File(FILE_JSON);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_JSON);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();

        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}