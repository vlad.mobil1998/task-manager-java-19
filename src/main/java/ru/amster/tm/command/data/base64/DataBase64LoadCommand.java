package ru.amster.tm.command.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.data.AbstractDataCommand;
import ru.amster.tm.dto.Domain;
import ru.amster.tm.enamuration.Role;
import sun.misc.BASE64Decoder;

import java.io.*;

public class DataBase64LoadCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String name() {
        return "data-base64-load";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Load base64 data to file";
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException {
        System.out.println("[DATA BASE64 LOAD]");

        @NotNull final File file = new File(FILE_BASE64);
        @NotNull final FileInputStream fileInputStream = new FileInputStream(file);

        @NotNull final byte[] base64 = new BASE64Decoder().decodeBuffer(fileInputStream);
        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(base64);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);

        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        fileInputStream.close();
        objectInputStream.close();
        byteArrayInputStream.close();

        serviceLocator.getAuthService().logout();
        userActivated.setCheckForActivation(false);

        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}