package ru.amster.tm.command.data.xml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.data.AbstractDataCommand;
import ru.amster.tm.dto.Domain;
import ru.amster.tm.enamuration.Role;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class DataXmlLoadCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String name() {
        return "data-xml-load";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Load xml data to file";
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException {
        System.out.println("[DATA XML LOAD]");

        @NotNull final File file = new File(FILE_XML);
        @NotNull final FileInputStream fileInputStream = new FileInputStream(file);

        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(file, Domain.class);

        setDomain(domain);
        fileInputStream.close();

        serviceLocator.getAuthService().logout();
        userActivated.setCheckForActivation(false);

        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}