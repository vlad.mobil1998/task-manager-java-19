package ru.amster.tm.command.data.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.data.AbstractDataCommand;
import ru.amster.tm.enamuration.Role;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class DataJsonClearCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String name() {
        return "data-json-clear";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Remove json data";
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException {
        System.out.println("[DATA JSON CLEAR]");
        @NotNull final File file = new File(FILE_JSON);
        Files.delete(file.toPath());
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}