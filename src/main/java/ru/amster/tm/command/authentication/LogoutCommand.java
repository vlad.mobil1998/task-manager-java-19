package ru.amster.tm.command.authentication;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.exception.user.AccessDeniedException;

public class LogoutCommand extends AbstractAuthCommand {

    @Override
    @NotNull
    public String name() {
        return "logout";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Log out system";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        if (!userActivated.getCheckForActivation()) throw new AccessDeniedException();
        serviceLocator.getAuthService().logout();
        userActivated.setCheckForActivation(false);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}