package ru.amster.tm.command.authentication;

public class UserActivated {

    private boolean checkForActivation = false;

    public boolean getCheckForActivation() {
        return checkForActivation;
    }

    public void setCheckForActivation(boolean checkForActivation) {
        this.checkForActivation = checkForActivation;
    }

}