package ru.amster.tm.command;

import lombok.NoArgsConstructor;
import ru.amster.tm.api.servise.ServiceLocator;
import ru.amster.tm.enamuration.Role;

import java.io.IOException;

@NoArgsConstructor
public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public Role[] roles() {
        return null;
    }

    public abstract String name();

    public abstract String arg();

    public abstract String description();

    public abstract void execute() throws IOException, ClassNotFoundException;

}