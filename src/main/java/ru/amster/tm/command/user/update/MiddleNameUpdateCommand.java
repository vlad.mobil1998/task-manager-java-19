package ru.amster.tm.command.user.update;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.exception.empty.EmptyEmailException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public class MiddleNameUpdateCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "upd-middle-name";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - updating user middle name";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE MIDDLE NAME]");
        System.out.println("ENTER MIDDLE NAME");
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final String middleName = TerminalUtil.nextLine();
        if (middleName == null || middleName.isEmpty()) throw new EmptyEmailException();
        serviceLocator.getUserService().updateMiddleName(userId, middleName);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}