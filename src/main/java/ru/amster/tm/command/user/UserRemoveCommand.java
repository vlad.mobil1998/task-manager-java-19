package ru.amster.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.exception.empty.EmptyLoginException;
import ru.amster.tm.util.TerminalUtil;

public class UserRemoveCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "re-user";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Delete user by login";
    }

    @Override
    public void execute() {
        System.out.println("[DELETED USER]");
        System.out.println("ENTER LOGIN");
        @Nullable final String login = TerminalUtil.nextLine();
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        serviceLocator.getUserService().removeByLogin(login);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}