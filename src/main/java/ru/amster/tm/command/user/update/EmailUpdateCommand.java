package ru.amster.tm.command.user.update;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.exception.empty.EmptyEmailException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public class EmailUpdateCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "upd-email";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - updating user email";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE EMAIL]");
        System.out.println("ENTER EMAIL");
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final String email = TerminalUtil.nextLine();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        serviceLocator.getUserService().updateEmail(userId, email);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}