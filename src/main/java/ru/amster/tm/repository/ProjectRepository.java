package ru.amster.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.repository.IProjectRepository;
import ru.amster.tm.entity.Project;
import ru.amster.tm.exception.empty.EmptyProjectException;
import ru.amster.tm.exception.user.AccessDeniedException;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    @NotNull
    private final List<Project> projects = new ArrayList<>();

    @NotNull
    public List<Project> getProjects() {
        return projects;
    }

    @Override
    public void add(@NotNull final Project project) {
        projects.add(project);
    }

    public void removeAll() {
        projects.clear();
    }

    @Override
    public void remove(@Nullable final String userId, @NotNull final Project project) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (userId.equals(project.getUserId())) projects.remove(project);
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final List<Project> result = new ArrayList<>();
        for (@NotNull final Project project : projects) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        for (@NotNull final Project project : projects) {
            if (!userId.equals(project.getUserId())) continue;
            remove(userId, project);
            return;
        }
    }

    @NotNull
    @Override
    public Project findOneById(@Nullable final String userId, @NotNull final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable Project result =null;
        for (@NotNull final Project project : projects) {
            if (!id.equals(project.getId())) continue;
            result = project;
            break;
        }
        if (result == null) throw new EmptyProjectException();
        if (!userId.equals(result.getUserId())) throw new AccessDeniedException();
        return result;
    }

    @NotNull
    @Override
    public Project findOneByName(@Nullable final String userId, @NotNull final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable Project result = null;
        for (@NotNull final Project project : projects) {
            if (!name.equals(project.getName())) continue;
            result = project;
            break;
        }
        if (result == null) throw new EmptyProjectException();
        if (!userId.equals(result.getUserId())) throw new AccessDeniedException();
        return result;
    }

    @NotNull
    @Override
    public Project findOneByIndex(@Nullable final String userId, @NotNull final Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final Project project = projects.get(index);
        if (project == null) throw new EmptyProjectException();
        if (!userId.equals(project.getUserId())) throw new AccessDeniedException();
        return project;
    }

    @NotNull
    @Override
    public Project removeOneByName(@Nullable final String userId, @NotNull final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final Project project = findOneByName(userId, name);
        if (project == null) throw new EmptyProjectException();
        if (!userId.equals(project.getUserId())) throw new AccessDeniedException();
        remove(userId, project);
        return project;
    }

    @NotNull
    @Override
    public Project removeOneByIndex(@Nullable final String userId, @NotNull final Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new EmptyProjectException();
        if (!userId.equals(project.getUserId())) throw new AccessDeniedException();
        remove(userId, project);
        return project;
    }

    @NotNull
    @Override
    public Project removeOneById(@Nullable final String userId, @NotNull final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new EmptyProjectException();
        if (!userId.equals(project.getUserId())) throw new AccessDeniedException();
        remove(userId, project);
        return project;
    }

    @NotNull
    @Override
    public Integer numberOfAllProjects(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @NotNull final List<Project> result = findAll(userId);
        return result.size();
    }

}