package ru.amster.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.repository.IUserRepository;
import ru.amster.tm.entity.User;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyLoginException;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    @NotNull
    private final List<User> users = new ArrayList<>();

    @NotNull
    public List<User> getUsers() {
        return users;
    }

    @Override
    public void removeAll() {
        users.clear();
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public @NotNull void add(@NotNull final User user) {
        users.add(user);
    }

    @Nullable
    @Override
    public User findById(@NotNull final String id) {
        for (@NotNull final User user : users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        for (@NotNull final User user : users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @NotNull
    @Override
    public User removeUser(@NotNull final User user) {
        users.remove(user);
        return user;
    }

    @NotNull
    @Override
    public User removeById(@NotNull final String id) {
        @NotNull final User user = findById(id);
        if (user == null) throw new EmptyIdException();
        return removeUser(user);
    }

    @NotNull
    @Override
    public User removeByLogin(@NotNull final String login) {
        @NotNull final User user = findByLogin(login);
        if (user == null) throw new EmptyLoginException();
        return removeUser(user);
    }

}